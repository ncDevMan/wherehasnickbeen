import React from 'react'
import Layout from '../components/layout'

import Header from '../components/Header'
import Main from '../components/Main'
import Footer from '../components/Footer'

import axios from 'axios';

class IndexPage extends React.Component {
  /**
   * 
   * @param {*} props 
   */
  constructor(props) {
    super(props)
    this.state = {
      isArticleVisible: false,
      timeout: false,
      articleTimeout: false,
      article: '',
      loading: 'is-loading',
      showModal: false,
      stateToSearch: ''
    }
    this.handleOpenArticle = this.handleOpenArticle.bind(this)
    this.handleCloseArticle = this.handleCloseArticle.bind(this)
    this.handleMap = this.handleMap.bind(this)
    this.setWrapperRef = this.setWrapperRef.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);

    this.handleCloseModal = this.handleCloseModal.bind(this);
  }

  /**
   * 
   */
  componentDidMount () {
    this.timeoutId = setTimeout(() => {
        this.setState({loading: ''});
    }, 100);
    // document.addEventListener('mousedown', this.handleClickOutside);
    document.addEventListener('mousedown', this.handleMap);
  }

  
  componentWillUnmount () {
    if (this.timeoutId) {
        clearTimeout(this.timeoutId);
    }
    // document.removeEventListener('mousedown', this.handleClickOutside);
  }

  /**
   * 
   * @param {*} node 
   */
  setWrapperRef(node) {
    this.wrapperRef = node;
  }




  /**
   * 
   * @param {*} article 
   */
  handleOpenArticle(article) {

    this.setState({
      isArticleVisible: !this.state.isArticleVisible,
      article
    })

    setTimeout(() => {
      this.setState({
        timeout: !this.state.timeout
      })
    }, 325)

    setTimeout(() => {
      this.setState({
        articleTimeout: !this.state.articleTimeout
      })
    }, 350)

  }

  /**
   * Function handleMap()
   * 
   * @param {event} event 
   */
  handleMap(event){
    if(event.target.dataset.name){


      // We can set the map state to search right here
      this.setState({stateToSearch: event.target.dataset.name})
      
      
      axios.get(`https://www.mediazone.nickconn.net/pictures/nc`)
      .then(res => {

        console.log(res.data)

        // Open the modal right here too
        this.setState({
          showModal : !this.state.showModal, 
          mapData : res.data
        })

      })


      
      


      
    }
    
  }

  /**
   * 
   */
  handleCloseArticle() {

    this.setState({
      articleTimeout: !this.state.articleTimeout
    })

    setTimeout(() => {
      this.setState({
        timeout: !this.state.timeout
      })
    }, 325)

    setTimeout(() => {
      this.setState({
        isArticleVisible: !this.state.isArticleVisible,
        article: ''
      })
    }, 350)

  }

  /**
   * 
   * @param {*} event 
   */
  handleClickOutside(event) {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      if (this.state.isArticleVisible) {
        this.handleCloseArticle();
      }
    }
  }

  /**
   * 
   */
  handleCloseModal(){
    
    this.setState({showModal : !this.state.showModal})
    this.handleOpenArticle('intro')
    
  }

  render() {
    return (
      <Layout location={this.props.location}>
        <div className={`body ${this.state.loading} ${this.state.isArticleVisible ? 'is-article-visible' : ''}`}>
          <div id="wrapper">
            <Header onOpenArticle={this.handleOpenArticle} timeout={this.state.timeout} />
            <Main
              isArticleVisible={this.state.isArticleVisible}
              timeout={this.state.timeout}
              articleTimeout={this.state.articleTimeout}
              article={this.state.article}
              onCloseArticle={this.handleCloseArticle}
              onMapHandler={this.handleMap}
              setWrapperRef={this.setWrapperRef}
              showModal={this.state.showModal}
              onCloseModal={this.handleCloseModal}
            />
            <Footer timeout={this.state.timeout} />
          </div>
          <div id="bg"></div>
        </div>
      </Layout>
    )
  }
}

export default IndexPage
